#include <SDL.h>
#include <stdbool.h>

int W = 1000;
int H = 1000;
const size_t POINTS_RESERVE = 1000;
const size_t IN = 1000; // number of interpolated points

#define MODE_MASK    0b00111
#define MODE_SPLINE1 1
#define MODE_SPLINE2 2
#define MODE_SPLINE3 3
#define MODE_POINTS  0b01000
#define MODE_LINES   0b10000

int mode = MODE_SPLINE2 | MODE_POINTS;

typedef struct {
	double x;
	double y;
} Point;

struct Points {
	size_t reserved;
	size_t count;
	Point *p;
} P, IP; // real points and interpolated points

void add_point(struct Points *p, double x, double y)
{
	int N = p->count + 1;
	if (p->count >= p->reserved) {
		p->reserved += POINTS_RESERVE;
		p->p = reallocarray(p->p, p->reserved, sizeof(Point));
	}
	p->p[N - 1].x = x;
	p->p[N - 1].y = y;
	p->count = N;
}

void clear_points(struct Points *p)
{
	p->count = 0;
}

int cmp_point_x(const void *a, const void *b)
{
	Point *aa = (Point *)a;
	Point *bb = (Point *)b;
	if (aa->x < bb->x)	return -1;
	if (aa->x > bb->x)	return +1;
	return 0;
}

void sort_points_x(struct Points *p)
{
	qsort(p->p, p->count, sizeof(Point), cmp_point_x);
}

size_t size_t_min(size_t a, size_t b)
{
	if (a < b) return a;
	return b;
}

// src must be sorted
void interpolate_spline1_y(const struct Points *src, struct Points *dest, size_t N)
{
	clear_points(dest);
	if (src->count < 2)
		return;

	size_t nspl = src->count - 1;
	size_t r = N % nspl;

	for (size_t i = 0; i < nspl; ++i) { // i - current spline
		size_t start = size_t_min(i, r) + (N / nspl) * i;
		size_t end = size_t_min(i + 1, r) + (N / nspl) * (i + 1);
		double ilen = end - start;
		double dx = (src->p[i + 1].x - src->p[i].x)/(double)(ilen);
		for (size_t j = 0; j < ilen; ++j) {
			double spl_x = src->p[i].x + dx * j;
			double a = src->p[i].y;
			double xi = src->p[i].x;
			double h = src->p[i + 1].x - src->p[i].x;
			double b = (src->p[i + 1].y - src->p[i].y)/h;
			double spl_y = a + b * (spl_x - xi); // phi(x) = a + b * (x - x_i)
			add_point(dest, spl_x, spl_y);
		}
	}
}

// src must be sorted
void interpolate_spline2_y(const struct Points *src, struct Points *dest, size_t N)
{
	clear_points(dest);
	if (src->count < 2)
		return;

	size_t nspl = src->count - 1;
	size_t r = N % nspl;

	double b_i = 0;
	for (size_t i = 0; i < nspl; ++i) { // i - current spline
		size_t start = size_t_min(i, r) + (N / nspl) * i;
		size_t end = size_t_min(i + 1, r) + (N / nspl) * (i + 1);
		double ilen = end - start;
		double dx = (src->p[i + 1].x - src->p[i].x)/(double)(ilen);

		// b_{i+1} = -b_{i} + 2 * (a_{i+1} - a_{i})/h
		// c_i = (b_{i+1} - b_i)/h
		double a_i = src->p[i].y;
		double a_i1 = src->p[i+1].y;
		double h = src->p[i+1].x - src->p[i].x;
		double b_i1 = -b_i + 2 * (a_i1 - a_i)/h;
		double c_i = (b_i1 - b_i)/h;

		double xi = src->p[i].x;
		for (size_t j = 0; j < ilen; ++j) {
			double spl_x = src->p[i].x + dx * j;
			double diff = spl_x - xi;
			double spl_y = a_i + b_i * diff + c_i/2 * diff*diff; // phi(x) = a+b*(x-x_i)+c/2*(x-x_i)^2
			add_point(dest, spl_x, spl_y);
		}
		b_i = b_i1;
	}
}

// src must be sorted
void interpolate_spline3_y(const struct Points *src, struct Points *dest, size_t N)
{
	clear_points(dest);
	if (src->count < 2)
		return;

	size_t nspl = src->count - 1;
	size_t r = N % nspl;

	double *h = calloc(nspl, sizeof(double));
	double *A = calloc(nspl, sizeof(double));
	double *l = calloc(nspl+1, sizeof(double));
	double *u = calloc(nspl+1, sizeof(double));
	double *z = calloc(nspl+1, sizeof(double));
	double *c = calloc(nspl+1, sizeof(double));
	double *b = calloc(nspl, sizeof(double));
	double *d = calloc(nspl, sizeof(double));

	for (size_t i = 0; i < nspl; ++i)
		h[i] = src->p[i+1].x - src->p[i].x;
	for (size_t i = 1; i < nspl; ++i)
		A[i] = 3*(src->p[i+1].y - src->p[i].y)/h[i] - 3*(src->p[i].y - src->p[i-1].y)/h[i-1];

	l[0] = 1;
	z[0] = 0;
	u[0] = 0;

	for (size_t i = 1; i < nspl; ++i) {
		l[i] = 2*(src->p[i+1].x - src->p[i-1].x) - h[i-1]*u[i-1];
		u[i] = h[i]/l[i];
		z[i] = (A[i] - h[i-1]*z[i-1])/l[i];
	}

	l[nspl] = 1;
	z[nspl] = 0;
	c[nspl] = 0;

	for (int j = nspl - 1; j >= 0; --j) {
		c[j] = z[j] - u[j] * c[j+1];
		b[j] = (src->p[j+1].y - src->p[j].y) / h[j] - h[j] * (c[j+1] + 2*c[j]) / 3;
		d[j] = (c[j+1] - c[j]) / (3 * h[j]);
	}

	for (size_t i = 0; i < nspl; ++i) { // i - current spline
		size_t start = size_t_min(i, r) + (N / nspl) * i;
		size_t end = size_t_min(i + 1, r) + (N / nspl) * (i + 1);
		double ilen = end - start;
		double dx = (src->p[i + 1].x - src->p[i].x)/(double)(ilen);
		double a_i = src->p[i].y;
		for (size_t j = 0; j < ilen; ++j) {
			double diff = dx * j;
			double spl_x = src->p[i].x + diff;
			double spl_y = a_i + b[i] * diff + c[i]/2 * diff*diff + d[i]/6 * diff*diff*diff;
			add_point(dest, spl_x, spl_y);
		}
	}

	free(h);
	free(A);
	free(l);
	free(u);
	free(z);
	free(c);
	free(b);
	free(d);
}

void draw_point(SDL_Renderer *rend, int x, int y)
{
	int r = 3;
	SDL_Rect rect = {.x = x - r, .y = y - r, .w = 2*r, .h = 2*r};
	SDL_RenderFillRect(rend, &rect);
}

void print_points(const struct Points *p)
{
	SDL_Log("point list (%lu)", p->count);
	for (size_t i = 0; i < p->count; ++i) {
		SDL_Log("{%0.1lf, %0.1lf}", p->p[i].x, p->p[i].y);
	}
	SDL_Log("end point list (%lu)", p->count);
}

void draw(SDL_Renderer *r)
{
	SDL_SetRenderDrawColor(r, 32, 32, 96, 255);
	SDL_RenderClear(r);
	sort_points_x(&P);
	switch (mode & MODE_MASK) {
		case MODE_SPLINE1:
			interpolate_spline1_y(&P, &IP, IN);
			break;
		case MODE_SPLINE2:
			interpolate_spline2_y(&P, &IP, IN);
			break;
		case MODE_SPLINE3:
			interpolate_spline3_y(&P, &IP, IN);
			break;
		default:
			clear_points(&IP);
	}
	if (mode & MODE_POINTS) {
		SDL_SetRenderDrawColor(r, 255, 255, 96, 255);
		for (size_t i = 0; i < IP.count; ++i) {
			draw_point(r, (int)IP.p[i].x, (int)IP.p[i].y);
		}
	}
	if (mode & MODE_LINES) {
		SDL_SetRenderDrawColor(r, 96, 255, 96, 255);
		for (size_t i = 1; i < IP.count; ++i)
			SDL_RenderDrawLine(r, (int)IP.p[i-1].x, (int)IP.p[i-1].y,
					(int)IP.p[i].x, (int)IP.p[i].y);
	}
	SDL_SetRenderDrawColor(r, 255, 32, 96, 255);
	for (size_t i = 0; i < P.count; ++i) {
		draw_point(r, (int)P.p[i].x, (int)P.p[i].y);
	}
	SDL_RenderPresent(r);
}

void dispatch_mouse_click_event(SDL_Event e)
{
	switch (e.button.button) {
		case SDL_BUTTON_RIGHT:
			SDL_Log("clear %lu points", P.count);
			clear_points(&P);
			break;
		case SDL_BUTTON_MIDDLE:
			print_points(&P);
		break;
		case SDL_BUTTON_LEFT:
			SDL_Log("create point %d %d", e.button.x, e.button.y);
			add_point(&P, e.button.x, e.button.y);
		break;
	}
}

bool dispatch_key_event(SDL_Event e)
{
	bool quit = false;
	SDL_Keymod mod = SDL_GetModState();
	switch (e.key.keysym.sym) {
		case SDLK_q:
			if (mod & KMOD_LCTRL || mod & KMOD_RCTRL) 
				quit = true;
			break;
		case SDLK_l:
			mode ^= MODE_LINES;
			break;
		case SDLK_p:
			mode ^= MODE_POINTS;
			break;
		case SDLK_1:
			mode &= ~MODE_MASK;
			mode |= 1;
			break;
		case SDLK_2:
			mode &= ~MODE_MASK;
			mode |= 2;
			break;
		case SDLK_3:
			mode &= ~MODE_MASK;
			mode |= 3;
			break;
	}
	return quit;
}

bool dispatch_event(SDL_Event e)
{
	bool quit = false;
	switch (e.type) {
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_KEYDOWN:
			quit = dispatch_key_event(e);
			break;
		case SDL_MOUSEBUTTONUP:
			dispatch_mouse_click_event(e);
			break;
	}
	return quit;
}

void run_loop(SDL_Renderer *r, SDL_Window *w)
{
	bool quit = false;
	while (!quit) {
		SDL_Event e;
		SDL_WaitEvent(&e);
		SDL_GetWindowSize(w, &W, &H);
		quit = dispatch_event(e);
		draw(r);
	}
}

int main(void)
{
	SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO);

	SDL_Window *window;
	window = SDL_CreateWindow(
			"Spline Visualiser",
			SDL_WINDOWPOS_UNDEFINED,
			SDL_WINDOWPOS_UNDEFINED,
			W,
			H,
			SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE
			);
	SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
	SDL_SetRenderDrawBlendMode(renderer, SDL_BLENDMODE_BLEND);

	SDL_Log("press '1' and '2' to change spline degree");
	SDL_Log("press 'l' to toggle lines");
	SDL_Log("press 'p' to toggle points");
	run_loop(renderer, window);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);

	clear_points(&P);
	free(P.p);
	free(IP.p);
	SDL_Quit();
}
